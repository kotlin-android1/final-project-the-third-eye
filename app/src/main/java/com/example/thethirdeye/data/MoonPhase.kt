package com.example.thethirdeye.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class MoonPhase(
    @SerializedName("id") @Expose var id: Int,
    @SerializedName("name") @Expose var name: String,
    @SerializedName("description") @Expose var description: String,
    @SerializedName("keyword") @Expose var keyword: String,
)
