package com.example.thethirdeye.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class TarotCard(
    @SerializedName("id") @Expose var id: Int,
    @SerializedName("name") @Expose var name: String,
    @SerializedName("description") @Expose var description: String,
    @SerializedName("description_daily") @Expose var description_daily: String,
    @SerializedName("description_love") @Expose var description_love: String,
    @SerializedName("description_finance") @Expose var description_finance: String,
    @SerializedName("description_career") @Expose var description_career: String,
    @SerializedName("yes_no") @Expose var yes_no:String,
    @SerializedName("description_yes_no") @Expose var description_yes_no:String,
    @SerializedName("image") @Expose var image:String
//    var card_image: String,
)
