package com.example.thethirdeye

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.thethirdeye.data.TarotCard
import com.example.thethirdeye.databinding.FragmentDailyTarotBinding
import com.example.thethirdeye.databinding.FragmentYesNoAnswerBinding
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [YesNoAnswer.newInstance] factory method to
 * create an instance of this fragment.
 */
class YesNoAnswer : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var _binding: FragmentYesNoAnswerBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentYesNoAnswerBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding?.imageCard?.setImageResource(R.drawable.tarot_cards_poster_with_mystical_elements_moon_stars_vector_illustration_340687_265)

        var random_idx = (0..21).random()

        val jsonFileString = getJsonDataFromAsset(this.requireContext(), "tarotcard.json")
        if (jsonFileString != null) {
            Log.i("data", jsonFileString)
        }

        val gson = Gson()
        val listPersonType = object : TypeToken<List<TarotCard>>() {}.type

        var persons: List<TarotCard> = gson.fromJson(jsonFileString, listPersonType)
        persons.forEachIndexed { idx, data ->
            //Log.i("DataJson", "> Item $idx:\n$name")
            if (data.id == random_idx){
                binding?.textAnswer?.setText(data.yes_no)
                binding?.textAnswerDetail?.setText(data.description_yes_no)
                binding?.textName?.setText(data.name)
                Log.i("DataJsonRandomIndex", "> Item $idx:\n$data")
                show_image(data.id)
            }
        }



        //binding?.textDailyTarotDescription?.setText(readJson(this.requireContext()).toString())

    }

    fun show_image (idx :Int){
        when (idx) {
            0 -> binding?.imageCard?.setImageResource(R.drawable.the_fool)
            1 -> binding?.imageCard?.setImageResource(R.drawable.the_magician)
            2 -> binding?.imageCard?.setImageResource(R.drawable.the_high_priestess)
            3 -> binding?.imageCard?.setImageResource(R.drawable.the_empress)
            4 -> binding?.imageCard?.setImageResource(R.drawable.the_emperor)
            5 -> binding?.imageCard?.setImageResource(R.drawable.the_hierophant)
            6 -> binding?.imageCard?.setImageResource(R.drawable.the_lover)
            7 -> binding?.imageCard?.setImageResource(R.drawable.the_chariot)
            8 -> binding?.imageCard?.setImageResource(R.drawable.the_strength)
            9 -> binding?.imageCard?.setImageResource(R.drawable.the_hermit)
            10 -> binding?.imageCard?.setImageResource(R.drawable.wheel_of_fortune)
            11 -> binding?.imageCard?.setImageResource(R.drawable.justice)
            12 -> binding?.imageCard?.setImageResource(R.drawable.the_hanged_man)
            13 -> binding?.imageCard?.setImageResource(R.drawable.the_death)
            14 -> binding?.imageCard?.setImageResource(R.drawable.temperance)
            15 -> binding?.imageCard?.setImageResource(R.drawable.the_devil)
            16 -> binding?.imageCard?.setImageResource(R.drawable.the_tower)
            17 -> binding?.imageCard?.setImageResource(R.drawable.the_star)
            18 -> binding?.imageCard?.setImageResource(R.drawable.the_moon)
            19 -> binding?.imageCard?.setImageResource(R.drawable.the_sun)
            20 -> binding?.imageCard?.setImageResource(R.drawable.judgment)
            21 -> binding?.imageCard?.setImageResource(R.drawable.the_world)
            else -> { // Note the block
                binding?.imageCard?.setImageResource(R.drawable.daily)
            }
        }
    }


    fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment YesNoAnswer.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            YesNoAnswer().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}