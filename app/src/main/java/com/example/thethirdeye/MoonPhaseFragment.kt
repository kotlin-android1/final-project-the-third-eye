package com.example.thethirdeye

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.get
import androidx.fragment.app.Fragment
import com.example.thethirdeye.data.MoonPhase
import com.example.thethirdeye.data.TarotCard
import com.example.thethirdeye.databinding.FragmentMoonPhaseBinding
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.IOException
import java.lang.Math.floor
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Moon_Phase.newInstance] factory method to
 * create an instance of this fragment.
 */
class Moon_Phase : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var _binding: FragmentMoonPhaseBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMoonPhaseBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        val simpleDateFormat = SimpleDateFormat("EEE, MMM yy")
//        val currentDateAndTime: String = simpleDateFormat.format(Date())



        val today = Calendar.getInstance()

//        val dayOfMonth = SimpleDateFormat("d")
//        val currentdayOfMonth: Int = dayOfMonth.format(Date()).toInt()
//        //val currentdayOfMonth: Int = 10
//        Log.d("currentdayOfMonth ", currentdayOfMonth.toString())
//
//        val month = SimpleDateFormat("M")
//        val currentMonth: Int = month.format(Date()).toInt()
//        //val currentMonth: Int = 10
//        Log.d("currentMonth ", currentMonth.toString())
//
//        val year = SimpleDateFormat("yyyy")
//        val currentyear: Int = year.format(Date()).toInt()
//        //val currentyear: Int = 2022
//        Log.d("currentyear ", currentyear.toString())
//
//        val weekday = SimpleDateFormat("EEE")
//        val currentWeekday: String = weekday.format(Date())
//        Log.d("currentWeekday ", currentWeekday)

        //val jd = moonCal(currentMonth, currentdayOfMonth, currentyear)

        binding?.textLine?.visibility = View.GONE
        binding?.cardMoonPhase?.visibility = View.GONE
        binding?.titleText?.visibility = View.GONE
        binding?.descriptionText?.visibility = View.GONE

        binding?.datePicker?.init(
            today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH)

        ) { view, year, monthOfYear, dayOfMonth ->
            val month = monthOfYear + 1
            val jd = moonCal(month, dayOfMonth, year)

            Log.d("monthvalue ", month.toString())

            Log.d("jdvalue ", jd.toString())

            binding?.textLine?.visibility = View.VISIBLE
            binding?.cardMoonPhase?.visibility = View.VISIBLE
            binding?.titleText?.visibility = View.VISIBLE
            binding?.descriptionText?.visibility = View.VISIBLE


            val jsonFileString = getJsonDataFromAsset(this.requireContext(), "moonphase.json")
            if (jsonFileString != null) {
                Log.i("data", jsonFileString)
            }

            val gson = Gson()
            val listPersonType = object : TypeToken<List<MoonPhase>>() {}.type

            var persons: List<MoonPhase> = gson.fromJson(jsonFileString, listPersonType)
            persons.forEachIndexed { idx, data ->
                if (data.id == moonphase(jd)) {
                    binding?.textMoonPhase?.setText(data.name)
                    binding?.textMoonPhaseKeyword?.setText(data.keyword)
                    binding?.textMoonPhaseDescription?.setText(data.description)
                    show_image(data.id)
                }

            }


        }


//        val jd = moonCal(month, dayOfmonth, year)
//
//        Log.d("jdvalue ", jd.toString())
//
//
//        val jsonFileString = getJsonDataFromAsset(this.requireContext(), "moonphase.json")
//        if (jsonFileString != null) {
//            Log.i("data", jsonFileString)
//        }
//
//        val gson = Gson()
//        val listPersonType = object : TypeToken<List<MoonPhase>>() {}.type
//
//        var persons: List<MoonPhase> = gson.fromJson(jsonFileString, listPersonType)
//        persons.forEachIndexed { idx, data ->
//            if (data.id == moonphase(jd)) {
//                binding?.textMoonPhase?.setText(data.name)
//                binding?.textMoonPhaseKeyword?.setText(data.keyword)
//                binding?.textMoonPhaseDescription?.setText(data.description)
//                show_image(data.id)
//            }
//
//        }

    }

    fun moonCal(in_month: Int, in_day: Int, in_year: Int): Double {
        var month = in_month
        var year = in_year
        var day = in_day

        if (month <= 2) {
            year--
            month + 12
        }
        val a: Int = year / 100
        Log.d("cala= ", a.toString())
        val b: Int = 2 - a + (a / 4)
        Log.d("calb= ", b.toString())
        var jd =
            floor(((365.25 * (year + 4716)) + (30.6001 * (month + 1)) + day + b - 1524.5)).toInt()
        Log.d("jd1= ", jd.toString())
        var jd2 = (((jd - 2454000.98958) / 29.530588 * 4000) % 4000) / 1000
        Log.d("jd2= ", jd2.toString())
        return jd2
    }

    fun moonphase(jd: Double): Int {
        if (jd < 0.25) {
            Log.d("0 jd < 0.25 ", "new moon")
            return 0
        } else if (jd >= 0.25 && jd < 0.75) {
            Log.d("1 jd < 0.75 ", "The waxing moon")
            return 1
        } else if (jd >= 0.75 && jd < 1.25) {
            Log.d("2 jd < 1.25", "The first quarter")
            return 2
        } else if (jd >= 1.25 && jd < 1.75) {
            Log.d("3 jd < 1.75", "The waxing gibbous moon")
            return 3
        } else if (jd >= 1.78 && jd < 2.25) {
            Log.d("4 jd < 2.25", "The full moon")
            return 4
        } else if (jd >= 2.25 && jd < 2.75) {
            Log.d("5 jd < 2.75", "The waning gibbous moon")
            return 5
        } else if (jd >= 2.75 && jd < 3.25) {
            Log.d("6 jd < 3.25", "The third quarter")
            return 6
        } else if (jd >= 3.25 && jd < 3.75) {
            Log.d("7 jd < 3.75", "The waning moon")
            return 7
        } else {
            Log.d("8 else", "new moon")
            return 0
        }
    }

    fun show_image(idx: Int) {
        when (idx) {
            0 -> binding?.imageCard?.setImageResource(R.drawable.the_new_moon)
            1 -> binding?.imageCard?.setImageResource(R.drawable.the_waxing_moon)
            2 -> binding?.imageCard?.setImageResource(R.drawable.the_first_quarter)
            3 -> binding?.imageCard?.setImageResource(R.drawable.the_waxing_gibbous_moon)
            4 -> binding?.imageCard?.setImageResource(R.drawable.the_full_moon)
            5 -> binding?.imageCard?.setImageResource(R.drawable.the_waning_gibbous_moon)
            6 -> binding?.imageCard?.setImageResource(R.drawable.the_third_quarter)
            7 -> binding?.imageCard?.setImageResource(R.drawable.the_waning_moon)
            else -> { // Note the block
                binding?.imageCard?.setImageResource(R.drawable.the_new_moon)
            }
        }
    }


    fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Moon_Phase.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Moon_Phase().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}