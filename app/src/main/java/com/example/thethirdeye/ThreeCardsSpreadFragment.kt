package com.example.thethirdeye

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.thethirdeye.data.TarotCard
import com.example.thethirdeye.databinding.FragmentDailyTarotBinding
import com.example.thethirdeye.databinding.FragmentThreeCardsSpreadBinding
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ThreeCardsSpread.newInstance] factory method to
 * create an instance of this fragment.
 */
class ThreeCardsSpread : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var _binding: FragmentThreeCardsSpreadBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentThreeCardsSpreadBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ////////// Time Header //////////

        val simpleDateFormat = SimpleDateFormat("EEE, MMM yy")
        val currentDateAndTime: String = simpleDateFormat.format(Date())
        binding?.textDate?.setText(currentDateAndTime)

        ////////// Random number //////////

        var random_idx = mutableListOf(
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            1,
            18,
            19,
            20,
            21
        )

        var card_random: MutableList<Int> = ArrayList()

        for (i in 0..2) {
            random_idx.shuffle()
            var random_card = random_idx.random()
            random_idx.removeAt(random_idx.indexOf(random_idx.find { it == random_card }))
            card_random.add(random_card)
        }
        Log.d("card_random", card_random.toString())


        ////////// Read Card //////////

        val jsonFileString = getJsonDataFromAsset(this.requireContext(), "tarotcard.json")
        if (jsonFileString != null) {
            Log.i("data", jsonFileString)
        }

        val gson = Gson()
        val listPersonType = object : TypeToken<List<TarotCard>>() {}.type

        var persons: List<TarotCard> = gson.fromJson(jsonFileString, listPersonType)


        ////////// Name Header //////////

        var choose_option = ThreeCardsSpreadArgs.fromBundle(requireArguments()).chooseOption

        if (choose_option == 1) {
            binding?.textDailyTarot?.setText("About Love")
            persons.forEachIndexed { idx, data ->
                //Log.i("DataJson", "> Item $idx:\n$name")
                if (data.id == card_random[0]) {
                    Log.i("DataJsonRandomIndex", "> Item $idx:\n$data")
                    binding?.textNameCardInThePast?.setText(data.name)
                    binding?.textDailyTarotDescriptionInThePast?.setText(data.description_love)
                    show_image_1(data.id)
                }
            }

            persons.forEachIndexed { idx, data ->
                //Log.i("DataJson", "> Item $idx:\n$name")
                if (data.id == card_random[1]) {
                    Log.i("DataJsonRandomIndex", "> Item $idx:\n$data")
                    binding?.textNameCardAtPresent?.setText(data.name)
                    binding?.textDailyTarotDescriptionAtPresent?.setText(data.description_love)
                    show_image_2(data.id)
                }
            }

            persons.forEachIndexed { idx, data ->
                //Log.i("DataJson", "> Item $idx:\n$name")
                if (data.id == card_random[2]) {
                    Log.i("DataJsonRandomIndex", "> Item $idx:\n$data")
                    binding?.textNameCardInTheFuture?.setText(data.name)
                    binding?.textDailyTarotDescriptionInTheFuture?.setText(data.description_love)
                    show_image_3(data.id)
                }
            }
        } else if (choose_option == 2) {
            binding?.textDailyTarot?.setText("About Finance")
            persons.forEachIndexed { idx, data ->
                //Log.i("DataJson", "> Item $idx:\n$name")
                if (data.id == card_random[0]) {
                    Log.i("DataJsonRandomIndex", "> Item $idx:\n$data")
                    binding?.textNameCardInThePast?.setText(data.name)
                    binding?.textDailyTarotDescriptionInThePast?.setText(data.description_finance)
                    show_image_1(data.id)
                }
            }

            persons.forEachIndexed { idx, data ->
                //Log.i("DataJson", "> Item $idx:\n$name")
                if (data.id == card_random[1]) {
                    Log.i("DataJsonRandomIndex", "> Item $idx:\n$data")
                    binding?.textNameCardAtPresent?.setText(data.name)
                    binding?.textDailyTarotDescriptionAtPresent?.setText(data.description_finance)
                    show_image_2(data.id)
                }
            }

            persons.forEachIndexed { idx, data ->
                //Log.i("DataJson", "> Item $idx:\n$name")
                if (data.id == card_random[2]) {
                    Log.i("DataJsonRandomIndex", "> Item $idx:\n$data")
                    binding?.textNameCardInTheFuture?.setText(data.name)
                    binding?.textDailyTarotDescriptionInTheFuture?.setText(data.description_finance)
                    show_image_3(data.id)
                }
            }
        } else if (choose_option == 3) {
            binding?.textDailyTarot?.setText("About Career")
            persons.forEachIndexed { idx, data ->
                //Log.i("DataJson", "> Item $idx:\n$name")
                if (data.id == card_random[0]) {
                    Log.i("DataJsonRandomIndex", "> Item $idx:\n$data")
                    binding?.textNameCardInThePast?.setText(data.name)
                    binding?.textDailyTarotDescriptionInThePast?.setText(data.description_career)
                    show_image_1(data.id)
                }
            }

            persons.forEachIndexed { idx, data ->
                //Log.i("DataJson", "> Item $idx:\n$name")
                if (data.id == card_random[1]) {
                    Log.i("DataJsonRandomIndex", "> Item $idx:\n$data")
                    binding?.textNameCardAtPresent?.setText(data.name)
                    binding?.textDailyTarotDescriptionAtPresent?.setText(data.description_career)
                    show_image_2(data.id)
                }
            }

            persons.forEachIndexed { idx, data ->
                //Log.i("DataJson", "> Item $idx:\n$name")
                if (data.id == card_random[2]) {
                    Log.i("DataJsonRandomIndex", "> Item $idx:\n$data")
                    binding?.textNameCardInTheFuture?.setText(data.name)
                    binding?.textDailyTarotDescriptionInTheFuture?.setText(data.description_career)
                    show_image_3(data.id)
                }
            }
        }


    }

    fun show_image_1 (idx :Int){
        when (idx) {
            0 -> binding?.imageCard1?.setImageResource(R.drawable.the_fool)
            1 -> binding?.imageCard1?.setImageResource(R.drawable.the_magician)
            2 -> binding?.imageCard1?.setImageResource(R.drawable.the_high_priestess)
            3 -> binding?.imageCard1?.setImageResource(R.drawable.the_empress)
            4 -> binding?.imageCard1?.setImageResource(R.drawable.the_emperor)
            5 -> binding?.imageCard1?.setImageResource(R.drawable.the_hierophant)
            6 -> binding?.imageCard1?.setImageResource(R.drawable.the_lover)
            7 -> binding?.imageCard1?.setImageResource(R.drawable.the_chariot)
            8 -> binding?.imageCard1?.setImageResource(R.drawable.the_strength)
            9 -> binding?.imageCard1?.setImageResource(R.drawable.the_hermit)
            10 -> binding?.imageCard1?.setImageResource(R.drawable.wheel_of_fortune)
            11 -> binding?.imageCard1?.setImageResource(R.drawable.justice)
            12 -> binding?.imageCard1?.setImageResource(R.drawable.the_hanged_man)
            13 -> binding?.imageCard1?.setImageResource(R.drawable.the_death)
            14 -> binding?.imageCard1?.setImageResource(R.drawable.temperance)
            15 -> binding?.imageCard1?.setImageResource(R.drawable.the_devil)
            16 -> binding?.imageCard1?.setImageResource(R.drawable.the_tower)
            17 -> binding?.imageCard1?.setImageResource(R.drawable.the_star)
            18 -> binding?.imageCard1?.setImageResource(R.drawable.the_moon)
            19 -> binding?.imageCard1?.setImageResource(R.drawable.the_sun)
            20 -> binding?.imageCard1?.setImageResource(R.drawable.judgment)
            21 -> binding?.imageCard1?.setImageResource(R.drawable.the_world)
            else -> { // Note the block
                binding?.imageCard1?.setImageResource(R.drawable.daily)
            }
        }
    }

    fun show_image_2 (idx :Int){
        when (idx) {
            0 -> binding?.imageCard2?.setImageResource(R.drawable.the_fool)
            1 -> binding?.imageCard2?.setImageResource(R.drawable.the_magician)
            2 -> binding?.imageCard2?.setImageResource(R.drawable.the_high_priestess)
            3 -> binding?.imageCard2?.setImageResource(R.drawable.the_empress)
            4 -> binding?.imageCard2?.setImageResource(R.drawable.the_emperor)
            5 -> binding?.imageCard2?.setImageResource(R.drawable.the_hierophant)
            6 -> binding?.imageCard2?.setImageResource(R.drawable.the_lover)
            7 -> binding?.imageCard2?.setImageResource(R.drawable.the_chariot)
            8 -> binding?.imageCard2?.setImageResource(R.drawable.the_strength)
            9 -> binding?.imageCard2?.setImageResource(R.drawable.the_hermit)
            10 -> binding?.imageCard2?.setImageResource(R.drawable.wheel_of_fortune)
            11 -> binding?.imageCard2?.setImageResource(R.drawable.justice)
            12 -> binding?.imageCard2?.setImageResource(R.drawable.the_hanged_man)
            13 -> binding?.imageCard2?.setImageResource(R.drawable.the_death)
            14 -> binding?.imageCard2?.setImageResource(R.drawable.temperance)
            15 -> binding?.imageCard2?.setImageResource(R.drawable.the_devil)
            16 -> binding?.imageCard2?.setImageResource(R.drawable.the_tower)
            17 -> binding?.imageCard2?.setImageResource(R.drawable.the_star)
            18 -> binding?.imageCard2?.setImageResource(R.drawable.the_moon)
            19 -> binding?.imageCard2?.setImageResource(R.drawable.the_sun)
            20 -> binding?.imageCard2?.setImageResource(R.drawable.judgment)
            21 -> binding?.imageCard2?.setImageResource(R.drawable.the_world)
            else -> { // Note the block
                binding?.imageCard2?.setImageResource(R.drawable.daily)
            }
        }
    }

    fun show_image_3 (idx :Int){
        when (idx) {
            0 -> binding?.imageCard3?.setImageResource(R.drawable.the_fool)
            1 -> binding?.imageCard3?.setImageResource(R.drawable.the_magician)
            2 -> binding?.imageCard3?.setImageResource(R.drawable.the_high_priestess)
            3 -> binding?.imageCard3?.setImageResource(R.drawable.the_empress)
            4 -> binding?.imageCard3?.setImageResource(R.drawable.the_emperor)
            5 -> binding?.imageCard3?.setImageResource(R.drawable.the_hierophant)
            6 -> binding?.imageCard3?.setImageResource(R.drawable.the_lover)
            7 -> binding?.imageCard3?.setImageResource(R.drawable.the_chariot)
            8 -> binding?.imageCard3?.setImageResource(R.drawable.the_strength)
            9 -> binding?.imageCard3?.setImageResource(R.drawable.the_hermit)
            10 -> binding?.imageCard3?.setImageResource(R.drawable.wheel_of_fortune)
            11 -> binding?.imageCard3?.setImageResource(R.drawable.justice)
            12 -> binding?.imageCard3?.setImageResource(R.drawable.the_hanged_man)
            13 -> binding?.imageCard3?.setImageResource(R.drawable.the_death)
            14 -> binding?.imageCard3?.setImageResource(R.drawable.temperance)
            15 -> binding?.imageCard3?.setImageResource(R.drawable.the_devil)
            16 -> binding?.imageCard3?.setImageResource(R.drawable.the_tower)
            17 -> binding?.imageCard3?.setImageResource(R.drawable.the_star)
            18 -> binding?.imageCard3?.setImageResource(R.drawable.the_moon)
            19 -> binding?.imageCard3?.setImageResource(R.drawable.the_sun)
            20 -> binding?.imageCard3?.setImageResource(R.drawable.judgment)
            21 -> binding?.imageCard3?.setImageResource(R.drawable.the_world)
            else -> { // Note the block
                binding?.imageCard3?.setImageResource(R.drawable.daily)
            }
        }
    }

    fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ThreeCardsSpread.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ThreeCardsSpread().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}